{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import RIO
    ( ($)
    , IO
    , logInfo
    , runSimpleApp
    )

main :: IO ()
main =
  runSimpleApp $ do
    logInfo "Hello World!"
